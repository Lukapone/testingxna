﻿using GDLibrary.Collision;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class Transform2D
    {
        //presentation related i.e. position, rotatation etc.
        private Vector2 position, origin, scale;
        private float rotation;
        private Matrix positionMatrix, originMatrix, rotationMatrix, scaleMatrix, world;

        //bounds related
        private Integer2 originalDimension;
        private Rectangle originalBounds, bounds;
        private bool isCollidable = true;


        //clean-dirty flags
        private bool isUpdated;


        #region Properties
        public bool IsCollidable 
        { 
            get
            {
                return isCollidable;
            }
            set
            {
                isCollidable = value;
            }
        }
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                positionMatrix = Matrix.CreateTranslation(new Vector3(position, 0));
                isUpdated = true;
            }
        }
      
        public float Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
                rotationMatrix = Matrix.CreateRotationZ(MathHelper.ToRadians(rotation)); //we want to convert to radians once or each time?
                isUpdated = true;
            }
        }
        public Vector2 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
                scaleMatrix = Matrix.CreateScale(scale.X, scale.Y, 1);
                isUpdated = true;
            }
        } 
        
        public Vector2 Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
                originMatrix = Matrix.CreateTranslation(new Vector3(-origin, 0));
                isUpdated = true;
            }
        }
        public Rectangle Bounds
        {
            get
            {
                return bounds;
            }
        }
        public Matrix World
        {
            get
            {
                return world;
            }
            protected set
            {
                world = value;
            }
        }
        public Matrix RotationMatrix
        {
            get
            {
                return rotationMatrix;
            }
        }
        public Matrix ScaleMatrix
        {
            get
            {
                return scaleMatrix;
            }
        }
        public Matrix PositionMatrix
        {
            get
            {
                return positionMatrix;
            }
        }
        #endregion

        public Transform2D(Vector2 position, float rotation, Vector2 scale, Vector2 origin, Integer2 originalDimension)
        {
            Position = position;
            Origin = origin;
            Scale = scale;
            Rotation = rotation;

            this.originalDimension = originalDimension;
            this.originalBounds = new Rectangle(0, 0, originalDimension.X, originalDimension.Y);

            //setup first time around
            updateWorld();
            updateBounds();
        }

        public Transform2D(Vector2 position, float rotation, Vector2 scale, Integer2 originalDimension)
            : this(position, rotation, scale, Vector2.Zero, originalDimension)
        {
          
        }

        public Transform2D(Vector2 position, Integer2 originalDimension)
            : this(position, 0, Vector2.One, Vector2.Zero, originalDimension)
        {

        }

        public Transform2D(Integer2 originalDimension)
            : this(Vector2.Zero, 0, Vector2.One, Vector2.Zero, originalDimension)
        {

        }

        public virtual void Update()
        {
            if(this.isUpdated)
            {
                this.isUpdated = false;

                updateWorld();

                updateBounds();

                //update bsp - to do...
            }
        }

        protected virtual void updateBounds()
        {
            //update bounds
            this.bounds = CollisionUtility.CalculateTransformedBoundingRectangle(originalBounds, world);
        }
        protected virtual void updateWorld()
        {
            //update world
            this.world = originMatrix * rotationMatrix * scaleMatrix * positionMatrix;
        }
    }
}
