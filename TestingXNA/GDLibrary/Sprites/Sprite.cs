﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TestingXNA;

namespace GDLibrary
{
    public class Sprite
    {
        #region Variables
        private static Main game;

        private Transform2D transform;
        private Texture2D texture;
        private Color color;
        private Rectangle sourceRectangle;
        private float depth;
        private SpriteEffects effects;

        #endregion

        #region Properties
        public static Main TheGame
        {
            get
            {
                return game;
            }
            set
            {
                game = value;
            }
        }

        public Transform2D Transform
        {
            get
            {
                return transform;
            }
            set
            {
                transform = value;
            }
        }
        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        public Rectangle SourceRectangle
        {
            get
            {
                return sourceRectangle;
            }
            set
            {
                sourceRectangle = value;
            }
        }
        public float Depth
        {
            get
            {
                return depth;
            }
            set
            {
                depth = value;
            }
        }
        public SpriteEffects Effects
        {
            get
            {
                return effects;
            }
            set
            {
                effects = value;
            }
        }
        #endregion

        public Sprite(Transform2D transform, Texture2D texture, Color color, float depth, Rectangle sourceRectangle, SpriteEffects effects)
        {
            this.transform = transform;

            this.texture = texture;
            this.color = color;
            this.sourceRectangle = sourceRectangle;
            this.effects = effects;
            this.depth = depth;
        }

        public Sprite(Transform2D transform, Texture2D texture, Color color, float depth)
            : this(transform, texture, color, depth, new Rectangle(0,0, texture.Width, texture.Height), SpriteEffects.None)
        {

        }
        public virtual void Update(GameTime gameTime)
        {
            transform.Update(); //must update the transform since this determines presentation properties
        }

        public virtual void Draw(GameTime gameTime, Main game)
        {
            game.SpriteBatch.Draw(texture, transform.Position, sourceRectangle, color, transform.Rotation, transform.Origin, transform.Scale, effects, depth);
        }
    }
}
