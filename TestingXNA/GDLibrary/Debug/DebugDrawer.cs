﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TestingXNA.GDLibrary.Debug
{
    public class DebugDrawer : DrawableGameComponent
    {
        private Main game;
        private Texture2D texture;
        private Color color;

        //temps
        private Sprite sprite;


        public DebugDrawer(Main game, Texture2D texture, Color color)
            : base(game)
        {
            this.game = game;
            this.texture = texture;
            this.color = color;
        }

        public override void Draw(GameTime gameTime)
        {
            game.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            for (int i = 0; i < game.SpriteManager.Count; i++)
            {
                this.sprite = game.SpriteManager[i];
                if(sprite.Transform.IsCollidable)
                {
                    game.SpriteBatch.Draw(texture, sprite.Transform.Bounds, color);
                }
            }
            game.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
