﻿namespace GDLibrary
{
    public enum GDContainmentType : byte
    {
        Contains,
        NotContainedLeft,
        NotContainedRight,
        NotContainedTop,
        NotContainedBottom
    }
}
