﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class Camera2D
    {
        private string name;
        private CameraTransform2D transform;
        private Viewport viewPort;

        public string Name
        {
            get
            {
                return name;
            }
        }
        public CameraTransform2D Transform
        {
            get
            {
                return transform;
            }
            set
            {
                transform = value;
            }
        }
        public Viewport Viewport
        {
            get
            {
                return viewPort;
            }
            set
            {
                viewPort = value;
                //this.Transform.ViewPortCentre = new Vector3(viewPort.Width / 2, viewPort.Height / 2, 0);
            }
        }
        public Camera2D(string name, CameraTransform2D transform, Viewport viewPort)
        {
            this.name = name;
            this.transform = transform;
            this.viewPort = viewPort;
        }


        public virtual void Update(GameTime gameTime)
        {

        }
    }
}
