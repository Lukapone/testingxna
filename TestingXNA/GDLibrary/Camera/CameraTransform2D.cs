﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class CameraTransform2D : Transform2D
    {
        private Vector3 viewPortCentre;

        public Vector3 ViewPortCentre
        {
            get
            {
                return viewPortCentre;
            }
        }
       
        public CameraTransform2D(Vector2 position, float rotation, Vector2 scale, Viewport viewPort)
            : base(position, rotation, scale, new Integer2(viewPort.Width, viewPort.Height))
        {
            this.IsCollidable = false;
            this.viewPortCentre = new Vector3(viewPort.Width / 2, viewPort.Height / 2, 0);
        }

        protected override void updateWorld()
        {
            this.World = Matrix.CreateTranslation(new Vector3(-this.Position, 0)) * this.RotationMatrix
                     * Matrix.CreateScale(this.Scale.X, this.Scale.Y, 1) * Matrix.CreateTranslation(this.viewPortCentre);
        }
    }
}
