﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    public class MoveableCamera2D : Camera2D
    {
        private Keys[] moveKeys;

        public MoveableCamera2D(string name, CameraTransform2D transform, Viewport viewPort, Keys[] moveKeys)
            : base(name, transform, viewPort)
        {
            this.moveKeys = moveKeys;
        }

        public override void Update(GameTime gameTime)
        {
            //add movement here
            base.Update(gameTime);
        }
    }
}
