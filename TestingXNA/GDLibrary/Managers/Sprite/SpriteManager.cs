﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TestingXNA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GDLibrary.Managers
{
    public class SpriteManager : DrawableGameComponent
    {
        private Main game;
        private List<Sprite> list;
        private bool bPause = false;


        public int Count
        { 
            get
            {
                return this.list.Count;
            }
        }
        public Sprite this[int index]
        {
            get
            {
                return this.list[index];
            }
        }



        public SpriteManager(Main game)
            : base(game)
        {
            this.game = game;
            this.list = new List<Sprite>();
        }

        public void add(Sprite sprite)
        {
            this.list.Add(sprite);
        }

        public void togglePause()
        {
            bPause = !bPause;
        }
        public override void Update(GameTime gameTime)
        {
            if (!bPause)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].Update(gameTime);
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //See http://msdn.microsoft.com/en-us/library/microsoft.xna.framework.graphics.blendstate_members.aspx 
            //See http://msdn.microsoft.com/en-us/library/microsoft.xna.framework.graphics.spritesortmode.aspx
            game.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);/*,
                SamplerState.LinearClamp, DepthStencilState.Default, RasterizerState.CullNone,
                null, game.ActiveCamera.Transform.World);*/
            for (int i = 0; i < list.Count; i++)
            {
                list[i].Draw(gameTime, game);
            }

            game.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
