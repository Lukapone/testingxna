﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using TestingXNA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers
{
    public class KeyboardManager : GameComponent
    {
        public KeyboardState newState, oldState;

        public KeyboardManager(Main game)
            : base(game)
        {
        }
        public override void Update(GameTime gameTime)
        {
            oldState = newState;
            newState = Keyboard.GetState();
            base.Update(gameTime);
        }
        public bool isFirstKeyPress(Keys key)
        {
            if (oldState.IsKeyUp(key) && newState.IsKeyDown(key))
                return true;

            return false;
        }

        public bool isKeyPressed()
        {
            if (newState.GetPressedKeys().Length != 0)
                return true;

            return false;

        }

        public bool IsKeyDown(Keys key)
        {
            return newState.IsKeyDown(key);
        }

        public bool IsKeyUp(Keys key)
        {
            return newState.IsKeyUp(key);
        }
    }
}
