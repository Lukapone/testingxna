﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using TestingXNA;

namespace GDLibrary.Managers
{
    public class CameraManager : GameComponent
    {
        private Main game;
        private List<Camera2D> list;
        private bool bPause = false;
        private int activeCameraIndex = 0;

        #region PROPERTIES
        public bool Pause
        {
            get
            {
                return bPause;
            }
            set
            {
                bPause = value;
            }
        }
        public Camera2D this[int index]
        {
            get
            {
                return list[index];
            }
        }
        public Camera2D this[string name]
        {
            get
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if(list[i].Name.Equals(name))
                        return list[i];
                }

                return null;
            }
        }

        public Camera2D ActiveCamera
        {
            get
            {
                return list[activeCameraIndex];
            }
        }

        public int ActiveCameraIndex
        {
            get
            {
                return activeCameraIndex;
            }
            set
            {

                activeCameraIndex = ((value >=0) && (value < list.Count)) ? value : 0;
            }
        }
        #endregion

        public CameraManager(Main game)
            : base(game)
        {
            this.list = new List<Camera2D>();
            this.game = game;
        }

        public void Add(Camera2D camera)
        {
            if (!list.Contains(camera))
                list.Add(camera);
        }
        public bool Remove(Camera2D camera)
        {
            return list.Remove(camera);
        }
        public int Size()
        {
            return list.Count;
        }

        public override void Update(GameTime gameTime)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].Update(gameTime);
            }
            base.Update(gameTime);
        }
    }
}






