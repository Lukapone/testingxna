﻿using Microsoft.Xna.Framework;
using TestingXNA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers
{
    public class ScreenManager : GameComponent
    {
        private Rectangle screenRectangle;

        #region Properties
        public Rectangle ScreenRectangle
        {
            get
            {
                return screenRectangle;
            }
        }
        public int ScreenWidth
        {
            get
            {
                return this.screenRectangle.Width;
            }
        }
        public int ScreenHeight
        {
            get
            {
                return this.screenRectangle.Height;
            }
        }
        #endregion

        public ScreenManager(Main game, int width, int height)
            : base(game)
        {
            this.screenRectangle = new Rectangle(0, 0, width, height);
            set(game, width, height);
        }

        public void set(Main game, int width, int height)
        {
            game.Graphics.PreferredBackBufferWidth = width;
            game.Graphics.PreferredBackBufferHeight = height;
            game.Graphics.ApplyChanges();
        }
    }
}
