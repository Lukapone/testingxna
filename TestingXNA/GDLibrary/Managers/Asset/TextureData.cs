﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TestingXNA;

namespace GDLibrary.Wrappers
{
    public class TextureData
    {
        #region Variables
        private string name;
        private Texture2D texture;
        private Color[] colorData;
        private bool isCollidable;
        private Integer2 dimensions;
        #endregion

        #region Properties
        public bool IsCollidable
        {
            get
            {
                return isCollidable;
            }
            set
            {
                isCollidable = value;

                if ((isCollidable) && (colorData == null))
                {
                    colorData = get1DColorData(this.texture);
                }
            }
        }
        public Color[] ColorData
        {
            get
            {
                return colorData;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            
        }
        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }
        }
        public Integer2 Dimensions
        {
            get
            {
                return dimensions;
            }
        }

        #endregion

        public TextureData(Main game, string path)
            : this(game, path, false)
        {
           
        }

        public TextureData(Main game, string path, bool isCollidable)
        {
            this.texture = game.Content.Load<Texture2D>(@"" + path);
            this.name = StringUtility.ParseNameFromPath(path);
            this.dimensions = new Integer2(texture.Width, texture.Height);

            //if collidable, then generate the color[] data for per-pixel collision test
            IsCollidable = isCollidable;
        }


        protected Color[] get1DColorData(Texture2D texture)
        {
            //read data into 1d array
            Color[] colorData = new Color[texture.Width * texture.Height];
            texture.GetData(colorData);
            return colorData;
        }

        protected Color[,] get2DColorData(Texture2D texture)
        {
            int width = texture.Width;
            int height = texture.Height;

            //read data into 1d array
            Color[] colors1D = new Color[width * height];
            texture.GetData(colors1D);

            //create 2d array to store data
            Color[,] colorData = new Color[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    colorData[x, y] = colors1D[x + y * width];
                }
            }

            return colorData;
        }
    }
}
















