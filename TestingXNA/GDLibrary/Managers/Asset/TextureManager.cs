﻿using GDLibrary.Wrappers;
using Microsoft.Xna.Framework.Graphics;
using TestingXNA;
using System.Collections.Generic;
using System;

namespace GDLibrary.Managers
{
    public class TextureManager
    {
        private Dictionary<string, TextureData> dictionary;

        public TextureData this[string name]
        {
            get
            {
                //Console.WriteLine("TEXTURE GETTER " + this.dictionary[name]);
                return this.dictionary[name];
            }
        }

        public TextureManager()
        {
            this.dictionary = new Dictionary<string, TextureData>();
        }
        public void Add(TextureData textureData)
        {
            if (!this.dictionary.ContainsKey(textureData.Name))
                this.dictionary.Add(textureData.Name, textureData);
                
            
        }

        public void Add(Main game, string[] paths)
        {
            foreach(string path in paths)
            {
                Add(new TextureData(game, path));
            }
        }



        public void Remove(string name)
        {
            if (this.dictionary.ContainsKey(name))
            {
                //once we call remove release texture data
                TextureData tData = this.dictionary[name];
                tData.Texture.Dispose();
                tData = null;

                this.dictionary.Remove(name);
            }
        }

        public int Count()
        {
            return this.dictionary.Count;
        }
        public void Clear()
        {
            //iterate through a dictionary
        }
    }
}
