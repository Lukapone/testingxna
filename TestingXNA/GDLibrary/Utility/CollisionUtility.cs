﻿using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using System;

namespace GDLibrary.Collision
{
    public class CollisionUtility
    {
        protected static float alphaThreshold = 10;

        public static GDContainmentType IsVerticallyContained(Rectangle container, Rectangle containee)
        {
            return IsVerticallyContained(container, containee, 0);
        }

        public static GDContainmentType IsVerticallyContained(Rectangle container, Rectangle containee, Vector2 moveVector)
        {
            return IsVerticallyContained(container, containee, moveVector, 0);
        }

        public static GDContainmentType IsVerticallyContained(Rectangle container, Rectangle containee, int padding)
        {
            if (containee.Top < container.Top + padding)
            {
                return GDContainmentType.NotContainedTop;
            }
            else if (containee.Bottom > container.Bottom - padding)
            {
                return GDContainmentType.NotContainedBottom;
            }

            return GDContainmentType.Contains;
        }

        public static GDContainmentType IsVerticallyContained(Rectangle container, Rectangle containee, Vector2 moveVector, int padding)
        {
            return IsVerticallyContained(container, GeometryUtility.ProjectRectangle(containee, moveVector), padding);
        }



        public static GDContainmentType IsHorizontallyContained(Rectangle container, Rectangle containee)
        {
            return IsHorizontallyContained(container, containee, 0);
        }

        public static GDContainmentType IsHorizontallyContained(Rectangle container, Rectangle containee, Vector2 moveVector)
        {
            return IsHorizontallyContained(container, containee, moveVector, 0);
        }

        public static GDContainmentType IsHorizontallyContained(Rectangle container, Rectangle containee, int padding)
        {
            if (containee.Left < container.Left + padding)
            {
                return GDContainmentType.NotContainedLeft;
            }
            else if (containee.Right > container.Right - padding)
            {
                return GDContainmentType.NotContainedRight;
            }

            return GDContainmentType.Contains;
        }

        public static GDContainmentType IsHorizontallyContained(Rectangle container, Rectangle containee, Vector2 moveVector, int padding)
        {
            return IsHorizontallyContained(container, GeometryUtility.ProjectRectangle(containee, moveVector), padding);
        }

        public static bool Intersects(Rectangle collidee, Rectangle collider, Vector2 moveVector)
        {
            Rectangle projectedCollider = GeometryUtility.ProjectRectangle(collider, moveVector);
            return projectedCollider.Intersects(collidee);
        }


        public static bool IntersectsAA(Color[] textureColorDataA, Color[] textureColorDataB,
                                      Rectangle boundsRectangleA, Rectangle boundsRectangleB)
        {
            // Find the extents of the rectangle intersection
            int top = Math.Max(boundsRectangleA.Top, boundsRectangleB.Top);
            int bottom = Math.Min(boundsRectangleA.Bottom, boundsRectangleB.Bottom);
            int left = Math.Max(boundsRectangleA.Left, boundsRectangleB.Left);
            int right = Math.Min(boundsRectangleA.Right, boundsRectangleB.Right);

            // Check every point within the intersection bounds
            for (int i = top; i < bottom; i++)
            {
                for (int j = left; j < right; j++)
                {
                    // Get the color of both pixels at this point
                    Color colorA = textureColorDataA[(j - boundsRectangleA.Left) +
                                         (i - boundsRectangleA.Top) * boundsRectangleA.Width];
                    Color colorB = textureColorDataB[(j - boundsRectangleB.Left) +
                                         (i - boundsRectangleB.Top) * boundsRectangleB.Width];

                    // If both pixels are not completely transparent,
                    if (colorA.A >= alphaThreshold && colorB.A >= alphaThreshold)
                    {
                        // then an intersection has been found
                        return true;
                    }
                }
            }

            // No intersection found
            return false;
        }

        public static Rectangle CalculateTransformedBoundingRectangle(Rectangle rectangle,
                                                           Matrix transform)
        {
            //   Matrix inverseMatrix = Matrix.Invert(transform);
            // Get all four corners in local space
            Vector2 leftTop = new Vector2(rectangle.Left, rectangle.Top);
            Vector2 rightTop = new Vector2(rectangle.Right, rectangle.Top);
            Vector2 leftBottom = new Vector2(rectangle.Left, rectangle.Bottom);
            Vector2 rightBottom = new Vector2(rectangle.Right, rectangle.Bottom);

            // Transform all four corners into work space
            Vector2.Transform(ref leftTop, ref transform, out leftTop);
            Vector2.Transform(ref rightTop, ref transform, out rightTop);
            Vector2.Transform(ref leftBottom, ref transform, out leftBottom);
            Vector2.Transform(ref rightBottom, ref transform, out rightBottom);

            // Find the minimum and maximum extents of the rectangle in world space
            Vector2 min = Vector2.Min(Vector2.Min(leftTop, rightTop),
                                      Vector2.Min(leftBottom, rightBottom));
            Vector2 max = Vector2.Max(Vector2.Max(leftTop, rightTop),
                                      Vector2.Max(leftBottom, rightBottom));

            // Return that as a rectangle
            return new Rectangle((int)Math.Round(min.X), (int)Math.Round(min.Y),
                                 (int)Math.Round(max.X - min.X), (int)Math.Round(max.Y - min.Y));
        }


    }
}
