﻿using Microsoft.Xna.Framework;
using System;

namespace GDLibrary.Utility
{
    public class MathUtility
    {
        public static Integer2 Round(Vector2 value)
        {
            return new Integer2(Math.Round(value.X), Math.Round(value.Y));
        }

        public static int RandomExcludeNumber(int excludedValue, int max)
        {
            Random random = new Random();
            int randomValue = 0;
            do
            {
                randomValue = random.Next(max);

            } while (randomValue == excludedValue);

            return randomValue;
        }

        public static int RandomExcludeRange(int lo, int hi, int max)
        {
            Random random = new Random();
            int randomValue = 0;
            do
            {
                randomValue = random.Next(max);

            } while ((randomValue >= lo) && (randomValue <= hi));

            return randomValue;
        }
    }
}
