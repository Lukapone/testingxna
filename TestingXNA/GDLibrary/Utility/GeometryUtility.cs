﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Utility
{
    public class GeometryUtility
    {
        public static Rectangle ProjectRectangle(Rectangle rectangle, Vector2 moveVector)
        {
            Rectangle projectedRectangle = rectangle;
            projectedRectangle.X += (int)Math.Round(moveVector.X);
            projectedRectangle.Y += (int)Math.Round(moveVector.Y);
            return projectedRectangle;
        }
    }
}
