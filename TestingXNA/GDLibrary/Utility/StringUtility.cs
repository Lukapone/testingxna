﻿using System.Text.RegularExpressions;

namespace GDLibrary.Utility
{
    public class StringUtility
    {
        public static string ParseNameFromPath(string path)
        {
            return Regex.Match(path, @"[^\\]*$").Value;
        }
    }
}
