﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestingXNA.GameClasses
{
    public class TachometerSprite : Sprite
    {



        public TachometerSprite(Transform2D transform, Texture2D texture, Color color, float depth, Rectangle sourceRectangle, SpriteEffects effects)
            : base(transform,texture,color,depth,sourceRectangle,effects)
        {

        }





        public override void Update(GameTime gameTime)
        {


            if (TheGame.KeyboardManager.isKeyPressed())
            {
                keyMovementPosition(gameTime);
                keyMovementOrigin(gameTime);
                keyMovementRotation(gameTime);
                keyMovementScale(gameTime);
            }

            //this.Transform.Position = new Vector2(300,300);
            //this.Transform.Rotation = 60;
            ////this.Transform.Origin = new Vector2(0, 0);
            //this.Transform.Origin = new Vector2(Texture.Width / 2, Texture.Height / 2);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, Main game)
        {
            base.Draw(gameTime, game);
        }

        private void keyMovementPosition(GameTime gameTime)
        {
         

            if (TheGame.KeyboardManager.IsKeyDown(Keys.Up))
            {
                this.Transform.Position += Vector2.UnitY * -1;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.Down))
            {
                this.Transform.Position += Vector2.UnitY * 1;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.Left))
            {
                this.Transform.Position += Vector2.UnitX * -1;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.Right))
            {
                this.Transform.Position += Vector2.UnitX * 1;
            }
        }
        private void keyMovementOrigin(GameTime gameTime)
        {

            if (TheGame.KeyboardManager.IsKeyDown(Keys.W))
            {
                this.Transform.Origin += Vector2.UnitY * -1;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.S))
            {
                this.Transform.Origin += Vector2.UnitY * 1;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.A))
            {
                this.Transform.Origin += Vector2.UnitX * -1;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.D))
            {
                this.Transform.Origin += Vector2.UnitX * 1;
            }
        }
        private void keyMovementScale(GameTime gameTime)
        {


            if (TheGame.KeyboardManager.IsKeyDown(Keys.T))
            {
                this.Transform.Scale += Vector2.UnitY * -0.1f;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.G))
            {
                this.Transform.Scale += Vector2.UnitY * 0.1f;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.F))
            {
                this.Transform.Scale += Vector2.UnitX * -0.1f;
            }
            if (TheGame.KeyboardManager.IsKeyDown(Keys.H))
            {
                this.Transform.Scale += Vector2.UnitX * 0.1f;
            }
          
        }
        private void keyMovementRotation(GameTime gameTime)
        {
            if (TheGame.KeyboardManager.IsKeyDown(Keys.R))
            {
                this.Transform.Rotation += 0.01f;
            }
            
        }

    }
}
