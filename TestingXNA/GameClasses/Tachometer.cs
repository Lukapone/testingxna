﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestingXNA.GameClasses
{
    public class Tachometer : DrawableGameComponent
    {
        List<TachometerSprite> list;

        private Main game;
        private bool bPause = false;


        public int Count
        { 
            get
            {
                return this.list.Count;
            }
        }
        public TachometerSprite this[int index]
        {
            get
            {
                return this.list[index];
            }
        }



        public Tachometer(Main game)
            : base(game)
        {
            this.game = game;
            this.list = new List<TachometerSprite>();
        }

        public void add(TachometerSprite sprite)
        {
            this.list.Add(sprite);
        }

        public void togglePause()
        {
            bPause = !bPause;
        }



        public void setTach()
        {
            for (int i = 0; i < list.Count; i++)
            {
                //list[i].Transform.Scale += Vector2.UnitX * 0.05f;
                list[i].Transform.Scale += Vector2.UnitY * i * 0.1f;




                list[i].Transform.Position += Vector2.UnitX * i * 50;
              


                //list[i].Transform.Rotation += 0.5f;
                //list[i].Transform.Origin += Vector2.UnitX ;
               // Console.WriteLine("POSITION " + i + list[i].Transform.Position.ToString());
               // Console.WriteLine("ORIGIN " +i+ list[i].Transform.Origin.ToString());
                
                //list[i].Transform.Origin += Vector2.UnitY * i * 50;
                //list[i].Transform.Origin = new Vector2(200, 200);
                //list[i].Transform.Rotation += 45f;
            }
        }

        float timer = 100;         //Initialize a 100 ms timer
        const float TIMER = 100;
        int counter = 0;

        public void accelerate(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            timer -= elapsed;
            if (timer < 0)
            {
                //Timer expired, execute action
                if (counter != list.Count)
                {
                    if (counter <= 0 || counter < 4)
                    {
                        list[counter].Color = Color.Green;

                    }
                    else if (counter < 5 || counter < 8)
                    {
                        list[counter].Color = Color.Orange;
                    }
                    else
                    {
                        list[counter].Color = Color.Red;
                    }
                    counter++;
                    timer = TIMER;   //Reset Timer

                }

            }
        }

        public void deAccelerate(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            timer -= elapsed;
            if (timer < 0)
            {
                //Timer expired, execute action
                if (counter != 0)
                {
                    counter--;
                    list[counter].Color = Color.White*0;
                    timer = TIMER;   //Reset Timer
                }
            }
        }
     

        public override void Initialize()
        {
            setTach();
            base.Initialize();
        }


        public override void Update(GameTime gameTime)
        {
           // Console.WriteLine("UPDATE");
           // list[counter].Color = Color.Green;
            if (game.KeyboardManager.IsKeyDown(Keys.X))
            {
                accelerate(gameTime);
            }
            if (game.KeyboardManager.IsKeyUp(Keys.X))
            {
                deAccelerate(gameTime);
            }






            if (!bPause)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].Update(gameTime);
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //See http://msdn.microsoft.com/en-us/library/microsoft.xna.framework.graphics.blendstate_members.aspx 
            //See http://msdn.microsoft.com/en-us/library/microsoft.xna.framework.graphics.spritesortmode.aspx
            game.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);/*,
                SamplerState.LinearClamp, DepthStencilState.Default, RasterizerState.CullNone,
                null, game.ActiveCamera.Transform.World);*/
            for (int i = 0; i < list.Count; i++)
            {
                list[i].Draw(gameTime, game);
            }

            game.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }

    
}
