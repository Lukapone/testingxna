﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using GDLibrary.Managers;
using TestingXNA.GDLibrary.Debug;
using GDLibrary;
using GDLibrary.Wrappers;
using TestingXNA.GameClasses;
using GDLibrary.Utility;
#endregion

namespace TestingXNA
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Game
    {
        
        #region Variables
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private ScreenManager screenManager;
        private KeyboardManager keyboardManager;
        private SpriteManager spriteManager;
        private DebugDrawer debugDrawer;
        private SpriteFont menuFont;
        private TextureManager textureManager;
        private CameraManager cameraManager;

        private Tachometer tachometer;
        #endregion
        #region Properties
        public Camera2D ActiveCamera
        {
            get
            {
                return cameraManager.ActiveCamera;
            }
        }
        public SpriteManager SpriteManager
        {
            get
            {
                return spriteManager;
            }
        }
        public KeyboardManager KeyboardManager
        {
            get
            {
                return keyboardManager;
            }
        }
        public ScreenManager ScreenManager
        {
            get
            {
                return screenManager;
            }
        }
        public GraphicsDeviceManager Graphics
        {
            get
            {
                return graphics;
            }
        }
        public SpriteBatch SpriteBatch
        {
            get
            {
                return spriteBatch;
            }
        }
        #endregion

        public Main()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

    
        protected override void Initialize()
        {
            setStaticVariables();

            loadManagers();

            loadAssets();

            loadTextures();

            loadSprites();

            loadCameras();

            loadDebug();

            base.Initialize();
        }

        private void loadCameras()
        {

        }


        private void loadDebug()
        {
            Texture2D debugTexture = Content.Load<Texture2D>(@"Images\Debug\debugrectangle");
            this.debugDrawer = new DebugDrawer(this, debugTexture, Color.Red);
            Components.Add(debugDrawer);
        }


        private void loadManagers()
        {
            this.screenManager = new ScreenManager(this, 800, 600);
            Components.Add(screenManager);

            this.keyboardManager = new KeyboardManager(this);
            Components.Add(keyboardManager);

            this.spriteManager = new SpriteManager(this);
            Components.Add(spriteManager);

            //simply a repository of references
            this.textureManager = new TextureManager();

            this.cameraManager = new CameraManager(this);
            Components.Add(cameraManager);

            this.tachometer = new Tachometer(this);
            Components.Add(tachometer);
        }

        private void setStaticVariables()
        {
            Sprite.TheGame = this;
        }

        private void loadSprites() //buttonTextureData.Dimensions
        {
            TextureData buttonTextureData = textureManager["button"];
            //Vector2 position, float rotation, Vector2 scale, Vector2 origin, Integer2 originalDimension
            Transform2D buttonTransform;
            TachometerSprite tachSprite;

            for(int i = 0 ; i < 13 ;i++)
            {
                //new Vector2(screenManager.ScreenWidth / 2, screenManager.ScreenHeight/2) origin in the middle of the screen
                //new Vector2(buttonTextureData.Texture.Width / 2, buttonTextureData.Texture.Height / 2) origin in the middle of the button
                buttonTransform = new Transform2D(new Vector2(0,0), 0f, new Vector2(0.5f,0.5f),
                new Vector2(buttonTextureData.Texture.Width / 2, buttonTextureData.Texture.Height / 2), buttonTextureData.Dimensions);
                buttonTransform = new Transform2D(new Vector2(100, 300), 0f, new Vector2(0.5f, 0.5f),
                new Vector2(buttonTextureData.Texture.Width / 2, buttonTextureData.Texture.Height / 2), buttonTextureData.Dimensions);

                tachSprite= new TachometerSprite(buttonTransform, buttonTextureData.Texture,
                Color.White*0, 0f, new Rectangle(0, 0, buttonTextureData.Texture.Width, buttonTextureData.Texture.Height), SpriteEffects.None);
                tachometer.add(tachSprite);
            }


        }

        private void loadAssets()
        {

        }
        private void loadFonts()
        {
         
        }

        private void loadTextures()
        {

            textureManager.Add(new TextureData(this, "Images\\button", false));
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            //Console.WriteLine("UPDATE");
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);
        }
    }
}
